from __future__ import print_function
from setuptools import setup, find_packages

setup(
  name='cactus',
  version=0.1,
  url='',
  license='MIT Software License',
  author='Ashok Aratikatla',
  author_email='ashokaratika@gmail.com',
  description='Cactus',
  packages=find_packages(),
  install_requires=[
    'requests>=2.19.1',
    'requests_toolbelt>=0.8.0'
  ],
  include_package_data=True,
  platforms='Linux',
  setup_requires=['pytest-runner'],
  tests_require=['pytest'],
  classifiers=['Programming Language :: Python',
               'Development Status :: 3 - Alpha',
               'Natural Language :: English',
               'Environment :: Console',
               'Intended Audience :: Developers',
               'License :: OSI Approved :: Apache Software License',
               'Operating System :: OS Independent',
               'Topic :: Software Development :: Libraries :: Python Modules'],
)
